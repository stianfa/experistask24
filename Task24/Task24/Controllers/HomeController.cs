﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.time = "The current time is: " + DateTime.Now.ToString("h:mm:ss tt");

            return View("MyFirstView");
        }

        public IActionResult SupervisorInfo()
        {
            List<Supervisor> sups = new List<Supervisor>()
            {
                new Supervisor { Name = "John", Id = 1 },
                new Supervisor { Name = "Lillian", Id = 2 },
                new Supervisor { Name = "Silje", Id = 3 }
            };

            return View(sups);
        }
    }
}
